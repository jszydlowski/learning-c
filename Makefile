CC=gcc

#x86_64-w64-mingw32
SDL_LIBS = -LC:/SDL2-2.0.3/i686-w64-mingw32/lib/ -lmingw32 -lSDL2main -lSDL2
SDL_INCLUDES = -I C:/SDL2-2.0.3/i686-w64-mingw32/include/
#SDL_LIBS = -LC:/SDL2-2.0.3/x86_64-w64-mingw32/lib/ -lmingw32 -lSDL2main -lSDL2
#SDL_INCLUDES = -IC:/SDL2-2.0.3/x86_64-w64-mingw32/include/

SDL_CFLAGS = -Wall -g $(SDL_INCLUDES) -mwindows  
CFLAGS = -Wall -g

all: ex my run 

cpu:
	$(CC) cpu\src\main.c -o cpu\bin\main.exe

ex: dbg ex1 ex3 ex5 ex6 ex7 ex8 ex9 ex10 ex11 ex14 ex15 ex16 ex19

ex19: ex19_object.o

my: my01_power quadratic dice ascii_square ascii_square2

sdl: sdl_test1

sdl_test1: sdl_test1.c
	$(CC) $(SDL_CFLAGS) $(SDL_LIBS) sdl_test1.c -o sdl_test1.exe
	
el: eller_maze
	./eller_maze

eller_maze: eller.o

eller.o: eller.h

run:
#	./dice	
#	./quadratic
#	./ex14 a b c d e

t: test
	./test

clean:
	rm -f dbg ex1 ex3 ex5 ex6 ex7 ex8 ex9 ex10 ex11 ex14 ex15 ex16 ex19
	rm -f my01_power dice quadratic test
	rm -f *.o



