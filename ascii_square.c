

int main(int argc, char* argv[])
{
	// read input
	int x = 0;
	printf("x=");
	scanf("%d", &x);

	// check input
	if (x < 1)
	{
		printf("x must be a integer >= 0\n");
	  	return 1;
	}

	// print square
	int i = 0;
	for (i = 0; i < x; ++i)
	{
		int j = 0;
		for (j = 0; j < x; ++j)
		{
			printf("x");
		}
		printf("\n");
	}

	return 0;
}

