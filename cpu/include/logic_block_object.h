#ifndef __logic_block_object_h
#define __logic_block_object_h

typedef enum
{
	LEFT,
	RIGTH,
	UP,
	DOWN
} Logic_block_direction;


typedef struct
{
	char id;
	unsigned int tick;

	Logic_block_direction return_true;
	Logic_block_direction return_false;

	Logic_block_direction (*do_tick)(void *self);	

} Logic_block_object;

Logic_block_direction Logic_block_object_tick(void *self);



#endif

