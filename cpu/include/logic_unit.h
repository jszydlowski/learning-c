#ifndef __logic_unit_h
#define __logic_unit_h

#include "include\logic_block_object.h"

typedef struct
{
	char width;
	char height;

	Logic_block_object *grid;	

} Logic_unit;

Logic_unit *Logic_unit_new(int width, int height);


#endif

