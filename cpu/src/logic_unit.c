#include "logic_unit.h"

Logic_unit *Logic_unit_new(int width, int height)
{
	Logic_unit *unit = calloc(1, sizeof(Logic_unit));
	if (unit)
	{
		unit->width = width;
		unit->height = height;
		
		unit->grid = calloc(width, sizeof(*Logic_unit));
		if (unit->grid)
		{
			int i = 0;
			for (; i < width; ++i)
			{
				unit->grid[i] = calloc(height, sizeof(*Logic_unit));
			}
		}


	}
	return unit;
}

