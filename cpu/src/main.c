#include <stdio.h>

#include "Logic_block_object.h"
#include "logic_unit.h"

int main(int argc, char* argv[])
{
	printf("====[ CPU START ]=======================================\n");

	Logic_unit *unit = Logic_unit_new(4, 3);
	printf("Logic_unit = %p\n", unit);


	printf("====[ CPU END ]=========================================\n");
	return 0;
}  
