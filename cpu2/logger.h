#ifndef __logger_h
#define __logger_h


#include <stdio.h>
#include <errno.h>
#include <string.h>

//#define log(M, ...)
#define log(M, ...) printf("LOG: %s:%d: " M "\n", __FILE__, __LINE__, ##__VA_ARGS__)


#endif /* __logger_h */

