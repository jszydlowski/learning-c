#ifndef __logic_block_h
#define __logic_block_h

#include <stdint.h>

typedef struct
{
	uint8_t id;
	uint16_t tick;
	uint8_t	(*dotick)(void *self);
} Logic_Block;

//default functions
Logic_Block *create_Logic_Block(uint8_t id);
uint8_t *Logic_Block_dotick(void *self);

#endif /* __logic_block_h */
