#include "logic_module.h"

Logic_Module *create_Logic_Module(int width, int height)
{
	Logic_Module *module = calloc(1, sizeof(Logic_Module));
	if (module)
	{
		module->module_base.status = 1;

		module->width = width;
		module->height = height;
		module->grid = calloc(module->width, sizeof(Logic_Block**));
		
		if (module->grid)
		{
			int i = 0;
			for (i = 0; i < module->width; ++i)
			{
				module->grid[i] = calloc(module->height, sizeof(Logic_Block*));
				if (!module->grid[i])
				{
					return NULL;
				}
				else
				{
					int j = 0;
					for (j = 0; j < module->height; ++j)
					{
						module->grid[i][j] = calloc(1, sizeof(Logic_Block));
						module->grid[i][j]->id = i * j;
						log("%d-%d 0x%p = %d ", i, j, module->grid[i][j],
								module->grid[i][j]->id);
					}
				}
			}
		}

		log("Module created at %p w=%d, h=%d\n", 
			module, module->width, module->height);
	}
	return module;
}

