#ifndef __logic_module_h
#define __logic_module_h

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#include "logger.h"
#include "module.h"
#include "logic_block.h"

typedef struct
{
	Module module_base;	

	uint8_t width;
	uint8_t height;

	Logic_Block ***grid;

} Logic_Module;

Logic_Module *create_Logic_Module(int width, int height);


#endif /* __logic_module_h */
