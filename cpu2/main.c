
#include <stdio.h>

#include "logic_module.h"


// turn ON debug logs
#include "logger.h"

int main(int argc, char* argv[])
{
	printf("----[ START ]--------------------------------------------------\n");

	Logic_Module *logic = create_Logic_Module(4, 3);

	// dummy loop
	int counter = 0;
	for (counter = 0; counter < 20; ++counter)
	{
		log("counter=%d", counter);		

	}

	printf("----[  END  ]--------------------------------------------------\n");
	return 0;
}


