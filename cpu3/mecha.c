
#include "mecha.h"

Mecha_Module *new_mecha_module(Mecha *mecha, uint8_t module_id)
{

	Mecha_Module *module = calloc(1, sizeof(Mecha_Module));
	if (module)
	{
		module->mecha = mecha;
		module->id = module_id;
	}
	else
	{
		module = NULL;
	}

	return module;
}


