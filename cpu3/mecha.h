#ifndef __mecha_h__
#define __mecha_h__

#include <stdint.h>
#include <stdlib.h>

typedef enum
{
	S_DEFAULT,
	S_OK,
	S_FAIL
} Status;

typedef struct Mecha Mecha;
typedef struct Mecha_Module Mecha_Module;

struct Mecha_Module
{
	Mecha *mecha;
	uint8_t id;
};

Mecha_Module_

Mecha_Module *new_mecha_module(Mecha *mecha, uint8_t module_id);




struct Mecha
{
	uint8_t player;
	// mecha parts
	uint8_t modules_count;
	Mecha_Module modules[];	
};

// interfaces

Status get_position(Mecha *mecha, int32_t *posx, int32_t *posy);
Status do_move(Mecha *mecha);
Status do_attack(Mecha *mecha);


#endif /* __mecha_h__ */

