#include "dbg.h"
#include <stdio.h>
#include <stdlib.h>

int test_function_2(int x)
{
	debug("Function without error label!");
	check(x < 5, "To high x (x=%d).", x);

	return 0;
error:
	return -1; 
}
 
int test_function(int x)
{
	debug("Function with error label!");
	int *y = malloc(sizeof(int));
	check_mem(y);
	*y = x;

	check(x < 5, "To high x (x=%d).", x);
 
	free(y);
	y = NULL;
	return 0;

error:
	debug("cleanup on error!");
	if(y) 
	{
		free(y);
		y = NULL;
	}
	return -1;
}


int main(int argc, char* argv[])
{
//	#define NDEBUG

	debug("test");
	log_err("some error");
	log_err("other %d errors", 4);
	log_warn("some warning");
	log_info("some info");

	test_function(1);
	test_function(6);


	return 1;
}
