#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <errno.h>
#include <assert.h>
#include <time.h>
#include <string.h>
#include <ctype.h>

//#define DEBUG

typedef unsigned int uint;

int roll_dice(int walls)
{
	return (rand() % walls)+1;
}

int roll_dices(int dices, int walls, int mod)
{
	#ifdef DEBUG
	printf("roll_dices(%d, %d, %d)\n", dices, walls, mod);
	#endif

	assert(walls > 0);
	assert(dices > 0);

	int result = 0;
	
	int i = 0;
	for (; i < dices; ++i)
	{
		result += roll_dice(walls);
	}
	
	result += mod;

	return result;
}


int read_and_parse_string(int *dices, int *walls, int *mod)
{
	int result = 0;

	const char k_char = 'k';

	char str[26];
	printf("please type requested dice in format xKy[+/-z]: ");
	scanf("%25s", str);
	printf("Parsing string: ""%s""\n", str);

	// parsing
	
	/* find 'k'
	 * /
	char *k_pos = strstr(str, k_char);
    printf("k_pos=%d (%p - %p)\n", k_pos - str, k_pos, str ); 
	// */

	// read x
//	int index = 0;
	char *pos = str;
	
	char xstr[10];
	char *xpos = xstr;

	while (isdigit(*pos))
	{
		*xpos = *pos;
		++pos;
		++xpos;
	}	

	*xpos = '\0';
	printf("xstr=%s ", xstr);

	if (*pos == k_char)
	{
		printf("%c ", *pos);
		++pos;
	}
	else
	{
		return 0;
	}
	

	// read y	
	char ystr[10];
	char *ypos = ystr;

	while (isdigit(*pos))
	{
		*ypos = *pos;
		++pos;
		++ypos;
	}	

	*ypos = '\0';
	printf("ystr=%s ", ystr);
	




	return result;
}


int read_and_parse(int *walls, int *dices, int *mod)
{
	assert(0 != walls);
	assert(0 != dices);
	assert(0 != mod);

	printf("please type requested dice in format xKy[+/-z]: ");
	int x = 0;
	int y = 0;
	char k[2];
	char sign[2];
	int z = 0;
	int read_result = scanf("%d%1[kK]%d%1[-+]%d", &x, k, &y, sign, &z);

	#ifdef DEBUG
	printf(" Readed[%d]: int[%d], char[%s], int[%d], char[%s], int[%d]\n", 
			read_result, x, k, y, sign, z);
	#endif

	if (read_result == 3 || read_result == 5)
	{
		*walls = x;
		*dices = y;
		*mod = z;

		if (0 == strcmp(sign, "-"))
		{
			*mod *= -1;
		}
		return 1;
	}

	return 0;
}

int test(uint walls)
{
	printf(" --- Test roll_dice() ---\n");	
	int *results = calloc((walls+2), sizeof(int));
	assert( results );
	int fails = 0;
	int i = 0;
	for (i = 0; i < 5000; ++i)
	{
		uint d = roll_dice(walls);
		results[d]++;
		assert(d >= 1 && d <= walls);
		printf("%d) %d - %d\n",i , d >= 1 && d <= walls, d );
	}

	for (i=0; i<walls+2; ++i)
	{
		printf("result[%d] = %d (%f%%)\n", i, results[i], results[i] / 5000.0);
	}

	free(results);
	results = NULL;
	return fails;
}


int main(int argc, char* argv[])
{
	srand(time(NULL));

//	test(20);
	int x = 0;
	int y = 0;
	int z = 0;


//	while (read_and_parse(&x, &y, &z))
	while (read_and_parse_string(&x, &y, &z))
	{
		printf("parsed: %d, %d, %d\n", x, y, z );
		printf( "result = %d\n", roll_dices(x, y, z));
	}

	return 0;
}

