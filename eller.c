#include "dbg.h"
#include "eller.h"

EllerRow *EllerRow_new(int size)
{
	check(size > 1, "Minimal size is 2.");
	

	EllerRow *row = calloc(1, sizeof(EllerRow));
	check_mem(row);

	row->size = size;
	row->cells = calloc(size, sizeof(EllerCell));
	check_mem(row->cells);

	int index = 0;
	for (index = 0; index < size; ++index)
	{
		row->cells[index].setid = 0;
		row->cells[index].walls = 0;
	}
	return row;

error:

	if (row)
	{
		if (row->cells) free(row->cells);
		free(row);
	}
	return NULL;
}
 

void EllerRow_delete(EllerRow *row)
{
	if (row)
	{
		if (row->cells) free(row->cells);
		free(row);
	}
}


EllerRow *EllerRow_copy(EllerRow *src)
{
	if (src)
	{
		int size = src->size;
		EllerRow *dst = EllerRow_new(size);
		check_mem(dst);
		
		int index = 0;
		for (index = 0; index < size; ++index)
		{
			dst->cells[index].setid = src->cells[index].setid;
			dst->cells[index].walls = src->cells[index].walls;
		}
		return dst;
	}

error:
	return NULL;
}

EllerRow *EllerRow_next(EllerRow *row)
{
	check(row, "Nullptr passed");

	// Prepara a copy of old row
	EllerRow *next = EllerRow_copy(row);
	check_mem(next);

	int index = 0;
	static int last_set_id = 1;

	// Step 5. Prepare base row
	// Step 5b. Remove right walls
	for (index = 0; index < next->size; ++index)
	{
//		next->cells[index].walls &= ~1;
		if (next->cells[index].walls & 2)
		{
			next->cells[index].setid = 0;
		}
		next->cells[index].walls = 0;
	}

	//Step 5c. Remove cells with bottom from sets
	

	//Step 2.
	//Join cells to a set
	for (index = 0; index < next->size; ++index)
	{
		if (next->cells[index].setid == 0)
		{
			next->cells[index].setid = last_set_id;
			last_set_id++;	
		}
	}	

	//Step 3.
	//Create right walls.
	for (index = 0; index < next->size-1; ++index)
	{
		printf("RW: [i]=%d [i+1]=%d",
			   next->cells[index].setid, next->cells[index+1].setid );
		if (next->cells[index].setid == next->cells[index+1].setid)
		{
			printf(" wall from set\n");
			next->cells[index].walls |= 1;
			continue;
		} 
		else if (rand() % 2 == 0)
		{
			//add wall
			printf( " wall from rand\n");
			next->cells[index].walls |= 1;			
		}
		else
		{
			printf(" merge sets\n");
			//union sets
			int i = 0;
			for (i = index+1; i < next->size; ++i)
			{
				if (next->cells[i].setid == next->cells[index+1].setid)
				{
					next->cells[i].setid = next->cells[index].setid;
				}
				else
				{
//					break;
				}
			}
		}
		
	}

	//Step 4.
	//Create bottom walls.
	for (index = 0; index < next->size; ++index)
	{
		int set_size = 0;
		int set_bottom_walls = 0;

		int j = 0;
		for (j = 0; j < next->size; ++j)
		{
			if (next->cells[index].setid == next->cells[j].setid)
			{
				set_size++;
				if (next->cells[j].walls & 2)
				{
					set_bottom_walls++;
				}
			}
		}


//		printf( "step4: set=%d size=%d, walls=%d\n", 
//				next->cells[index].setid, set_size, set_bottom_walls);
		if ( (set_size > 1) && 
			 (set_size > (set_bottom_walls +1)) )
		{
			if (rand() % 2 == 0)
			{
				next->cells[index].walls |= 2;
			}
		}
	}		

	return next;

error:
	return NULL;
}


void EllerRow_print(EllerRow *row)
{
	if (row)
	{
		int index = 0;
		for (index = 0; index < row->size; ++index)
		{
			EllerCell_print(&row->cells[index]);
		}
	}
}

void EllerRow_ascii_draw(EllerRow *row)
{
	if (row)
	{
		int i = 0;
		printf("|");
		for (i = 0; i < row->size; ++i)
		{
			switch (row->cells[i].walls)
			{
				case 0:
					printf("  ");
					break;
				case 1:
					printf(" |");
					break;
				case 2:
					printf("__");
					break;
				case 3:
					printf("_|");
					break;
				default:
					sentinel("Invalid walls value = %d.", row->cells[i].walls);
			}
		}
//		printf("|\n");
		printf("|\t");

		for (i = 0; i < row->size; ++i)
		{
			printf("%d ", row->cells[i].setid);
		}
		printf("\n");	

	}
error:
	(void) row;

}

void EllerRow_ascii_draw2(EllerRow *row)
{
	if (row)
	{
		int i = 0;
		printf("|");
		for (i = 0; i < row->size; ++i)
		{
			printf("%d", row->cells[i].setid);
			switch (row->cells[i].walls)
			{
				case 0:
					printf(".. ");
					break;
				case 1:
					printf(".| ");
					break;
				case 2:
					printf("__ ");
					break;
				case 3:
					printf("_| ");
					break;
				default:
					sentinel("Invalid walls value = %d.", row->cells[i].walls);
			}
		}
//		printf("|\n");
		printf("|\t");

		for (i = 0; i < row->size; ++i)
		{
			printf("%d ", row->cells[i].setid);
		}
		printf("\n");	

	}
error:
	(void) row;

}


void EllerCell_print(EllerCell *cell)
{
	if(cell)
	{
		printf( "Cell set=%d, walls=%d\n", cell->setid, cell->walls);
	}
}




