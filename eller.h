#include <stdlib.h>

struct EllerCell
{
	unsigned int setid;
	char walls;
};
typedef struct EllerCell EllerCell;

struct EllerRow
{
	EllerCell *cells;
	unsigned int size;
};
typedef struct EllerRow EllerRow;


EllerRow *EllerRow_new(int size);
void EllerRow_delete(EllerRow *row);
EllerRow *EllerRow_copy(EllerRow *src);
EllerRow *EllerRow_next(EllerRow *old_row);
void EllerRow_print(EllerRow *row);
void EllerRow_ascii_draw(EllerRow *row);

void EllerCell_print(EllerCell *cell);

