#include "dbg.h"
#include "eller.h"

#include <time.h>

int main(int argc, char* argv[])
{
	// RNG
	//srand(time(NULL));
	// Not to Random NG
	srand( 23 );

	//debug("Eller maze generator");
	int maze_size = 8;	
	EllerRow *row = EllerRow_new(maze_size);
	check_mem(row);
//	EllerRow_print(row);
	
	int i = 0;
	for (i = 0; i < maze_size; ++i)
	{
		
		row = EllerRow_next(row);
		//EllerRow_print(row);	
		EllerRow_ascii_draw(row);
	//	EllerRow_ascii_draw2(row);
	}
	
	EllerRow_delete(row);
	return 0;
error:
	EllerRow_delete(row);

	return 1;
}
