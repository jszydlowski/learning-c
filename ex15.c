#include <stdio.h>

int main(int argc, char *argv[])
{
	int ages[] = {23, 43, 12, 89, 2};
	char *names[] = {"Alan", "Frank", "Mary", "John", "Lisa"};

	// size of ages
	int count = sizeof(ages) / sizeof(int);	
	int i = 0;

	// WAY 1
	for (i = 0; i < count; ++i)
	{
		printf("%s has %d years alive.\n",
				names[i], ages[i]);
	}

	printf("---\n");


	int *cur_age = ages;
	char **cur_name = names;

	//WAY 2
	for (i = 0; i < count; ++i)
	{
		printf("%s is %d years old. [ptr name: %p ptr age: %p]\n",
				*(cur_name+i), *(cur_age+i),
				(cur_name+i), (cur_age+i));
	}

	printf("---\n");

	
	// WAY 3
	for (i = 0; i < count; ++i)
	{
		printf("%s is %d years old again.\n",
				cur_name[i], cur_age[i]);
	}

	printf("---\n");


	// WAY 4
	for (cur_name = names, cur_age = ages;
			(cur_age - ages) < count;
			cur_name++, cur_age++)
	{
		printf("%s lived %d years so far. [cur-ages: %ld]\n",
				*cur_name, *cur_age,
				(cur_age - ages));

	}

	printf("---\n");

	return 0;
}


