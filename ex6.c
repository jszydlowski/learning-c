#include <stdio.h>

int main( int argc, char *argv[])
{

	int distance = 100;
	float power = 2.345f;
	double super_power = 56789.4532;
	char initial = 'A';
	char first_name[] = "";
	char last_name[] = "S";

	printf( "int: %d\n", distance );
	printf( "float: %f\n", power );
	printf( "double: %f\n", super_power );
	printf( "char: %c\n", initial );
	printf( "char*1: %s\n", first_name );
	printf( "char*2: %s\n", last_name );
	printf( "combo: %s %c. %s.\n", first_name, initial, last_name );

	return 0;
}
