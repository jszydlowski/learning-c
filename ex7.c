#include <stdio.h>

int main(int argc, char* argv[])
{
	int bugs = 100;
	double bug_rate = 1.2;
	printf("%d bugs at rate %f.\n", bugs, bug_rate);


	long universe_of_defects = 1L * 1024L * 1024L * 1024L;
	// printf("Universe has %d bugs.\n", universe_of_defects); // WARNING
	printf("Long Universe has %ld bugs.\n", universe_of_defects);


	double expected_bugs = bugs * bug_rate;
	printf("You will have %f bugs.\n", expected_bugs);


	double part_of_universe = expected_bugs / universe_of_defects;
	printf("%e portion of universe.\n", part_of_universe);


	char nul_byte = '\0';
	int care_percentage = bugs * nul_byte;
	printf("You should care %d%%.\n", care_percentage);

	return 0;

}

