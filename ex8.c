#include <stdio.h>

int main(int argc, char* argv[])
{
	int areas[] = {10, 12, 13, 14, 20};
	char name[] = "Zed";
	char full_name[] = 
	{ 
		'Z', 'e', 'd', ' ', 'A', '.', ' ', 'S', 'h', 'a',
		'w', '\0' 
	};

	printf("Size of int: %ld\n", sizeof(int));
	printf("Size of areas: %ld\n", sizeof(areas) / sizeof(int));
	printf("First area %d, second %d\n", areas[10], areas[1]);

	printf("Size of char: %ld\n", sizeof(char));
	printf("Size of name: %ld\n", sizeof(name));
	printf("Number of chars: %ld\n", sizeof(name) / sizeof(char));

	printf("Size of full_name: %ld\n", sizeof(full_name));
	printf("Number of chars: %ld\n", sizeof(full_name) / sizeof(char));

	printf("name=\"%s\" and full_name=\"%s\"\n", name, full_name);

	return 0;
}


