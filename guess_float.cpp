// A simple number-guessing game using float types

#include <iostream>
#include <ctime>
#include <cstdlib>

using namespace std;

int main()
{
    //generate random
    srand(time(NULL));
    float x = rand() %100  +10;
    //test if it works
    // test cout << "Random number before division: " << x << endl;
    float x_fl = x / 10;
    float guess = 0;
    int counter = 0;
    // test if it works
    // cout << "Number to guess after division: " << x_fl << endl;

    cout << "I have chosen a number between 1 and 10 with decimals (e.g. 4.5). Guess: " << endl;
    do
    {
        cout << "Is it...? ";
        cin >> guess;
        counter = counter +1;
        if (guess > x_fl)
        {
            cout << "My number is smaller!" << endl;
        }
        else if (guess < x_fl)
        {
            cout << "My number is bigger!" << endl;
        }

    }
    while(guess != x_fl);
    cout << "You are right! It's " << x_fl << " and you guessed it in " << counter << " attempt(s)." << endl;

    return 0;
}