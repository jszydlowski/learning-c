/*
 * Author:			Jacek Szydlowski
 * Date:			2015-11-25
 * File:			my01_power.c
 * Description:		Prepare a functions that will calculate exponential in
 * 					iteration and recurence way.
 */

#include <stdio.h>

//#define DEBUG

// iteration
unsigned int ipower(unsigned int base, unsigned int exponent)
{
	unsigned int result = 1;

	int i = 0;
	for (i = 1; i <= exponent; ++i)
	{
		result *= base;
		#ifdef DEBUG
			printf("iteration %d: temp result: %d.\n", i, result);
		#endif /*DEBUG*/
	}

	printf("%d^%d = %d\n", base, exponent, result);
	return result;
}


// recurence
unsigned int rpower(unsigned int base, unsigned int exponent)
{
	unsigned int result = 0;

	#ifdef DEBUG
		printf("%d^%d call\n", base, exponent);
	#endif /*DEBUG*/

	if (0 == exponent)
	{
		result = 1;
	}
	else
	{
		result = base * rpower( base, exponent - 1 );
	}

	#ifdef DEBUG
		printf("%d^%d = %d\n", base, exponent, result);
	#endif /*DEBUG*/
	
	return result;
}


int main(int argc, char *argv[])
{
	#ifdef DEBUG
		printf("DEBUG MODE ON\n");
	#endif /*DEBUG*/

	ipower( 2, 2 );
	ipower( 2, 6 );
	ipower( 2, 0 );
	ipower( 7, 1 );
	ipower( 3, 3 );
	ipower( 4, 11 );

	printf("result = %d\n", rpower( 2, 2 ));
	printf("result = %d\n", rpower( 2, 6 ));
	printf("result = %d\n", rpower( 2, 0 ));
	printf("result = %d\n", rpower( 7, 1 ));
	printf("result = %d\n", rpower( 3, 3 ));
	printf("result = %d\n", rpower( 4, 11 ));


	return 0;
}

