/*******************************************************************************
 * 
 * Author:			Jacek Szydlowski
 * Date:			2015-11-26
 * File:			quadratic.c
 * Description:		Function for calculation of quadratic equation in form
 * 				    ax^2+bx+c=0 with proper printing.
 * 				    
 ******************************************************************************/

/******************************************************************* INCLUDES */
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <math.h>

/********************************************************************* MACROS */
#define DEBUG

#define MAX_STR 512

/******************************************************************** STRUCTS */
struct Number
{
	double real;
	double i;
};

/****************************************************************** FUNCTIONS */
struct Number *Number_new(double r, double i)
{
	struct Number *num = malloc(sizeof(struct Number));
	assert(NULL != num);

	num->real = r;
	num->i = i;

	return num;
}

void Number_print(struct Number *num)
{
	assert(NULL != num);

	if (0 == num->i)
	{
		printf("%g", num->real);
		return;
	}
	else if (0 < num->i)
	{
		printf("%g+%gi", num->real, num->i);
	}
	else
	{
		printf("%g%gi", num->real, num->i);
	}
}


int calculate_discriminant(int a, int b, int c)
{
	return b*b - (4*a*c);
}

void calculate_roots(int a, int b, int c )
{


}


void print_quadratic(int a, int b, int c)
{
//	char str[MAX_STR] = "";


}


int calculate_quadratic(int a, int b, int c,
						 struct Number *result1, struct Number *result2 )
{
	#ifdef DEBUG
	printf("calculate quadratic with a=%d, b=%d, c=%d and ptr1=%p, ptr2=%p\n",
			a, b, c, result1, result2);
	#endif

//	assert(NULL != result1);
//	assert(NULL != result2);

	int delta = calculate_discriminant(a, b, c);

	if (delta < 0)
	{
		// TODO complex numbers results
	}
	else if (delta == 0)
	{
		result1 = Number_new( -(b / (2*a)), 0 );
		result2 = NULL;
		#ifdef DEBUG
		printf("Delta == 0\nx1=");
		Number_print( result1 );
		printf( "%p - %f %f", result1, result1->real, result1->i );
		printf("\n");
		#endif
		return 1;
	}
	else if (delta > 0)
	{
		

	}
	else
	{
		assert(0);
	}
	

	return 0;
}

int *get_user_input(int *abc)
{
	printf("Please enter a: ");
	scanf("%d", &abc[0]);

	printf("Please enter b: ");
	scanf("%d", &abc[1]);

	printf("Please enter c: ");
	scanf("%d", &abc[2]);

	return abc;
}

/*********************************************************************** MAIN */

int main(int argc, char* argv[])
{
	printf("+----------------------------------------------------------------------+\n");
	printf("|                    Quadratic equation solver v1.0                    |\n");
	printf("+----------------------------------------------------------------------+\n");

	int *abc = malloc(sizeof(int)*3);
	assert(NULL != abc);

	get_user_input(abc);

//	struct Number x1;
//	struct Number x2;
	struct Number *x1 = malloc(sizeof(struct Number));
	struct Number *x2 = malloc(sizeof(struct Number));
	calculate_quadratic(abc[0], abc[1], abc[2], x1, x2);

	printf("Equation results:\n");
	if (NULL != x1)
	{
		printf("x1 = ");
		Number_print(x1);
		printf("\n");
	}

	if (NULL != x2)
	{
		printf("x1 = ");
		Number_print(x2);
		printf("\n");
	}


	if (NULL == abc)
	{
		free(abc);
		abc = NULL;
	}

	return 0;
}


