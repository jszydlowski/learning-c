#include <stdio.h>
#include <stdlib.h>

/*******************************************************************************
 * Table of contents:
 * - Polymorphic structs 
 * - Bit operations
 * - Address of table
 * - Function pointers
 * - Structures copying
 * - Passing pointers
 * - statin and non static char[]
 ******************************************************************************/

/*******************************************************************************
 * Polymorphic structs
 */
typedef struct
{
	int ai;
	char ac;
} A;

typedef struct
{
	A a;
	int bi;
	char bc;
} B;

typedef struct
{
	A a;
	int ci[20];
} C;


// poly
typedef struct
{
	int id;
	int (*dotick)(void *self);
} Obj;

typedef struct
{
	Obj obj;

	int i;
	char c;
} ClassA;

typedef struct
{
	Obj obj;

	float d;
	long int l;
} ClassB;



int main(int argc, char* argv[])
{
	Obj **tab;
	
	tab = calloc(4, sizeof(Obj*));

	tab[0] = calloc(1, sizeof(ClassA));
	tab[1] = calloc(1, sizeof(ClassA));
	tab[2] = calloc(1, sizeof(ClassB));
	tab[3] = calloc(1, sizeof(ClassB));

	ClassA *ca = (ClassA*)tab[0];
	ClassB *cb = (ClassB*)tab[0];
	
	cb->d = 5.0;
//	ca->i = 3;

	printf("%p-%p\t%d - %f\n",ca, cb, ca->i, cb->d);	

	return 0;
}

// */

/*******************************************************************************
 * Bit operations
 * /
typedef struct
{
	char l : 3;
	char r : 3;
	char u : 3;
	char d : 3;
} port;

int main(int argc, char* argv[])
{
	int a = 0;
	int b = a | 2;
	printf("%d\n", b);
	printf("%d\n", b & 1);

	port p;
	printf("sizeof port = %d\n", sizeof( port ));
	printf("sizeof short int = %d\n", sizeof( int ));

	return 0;
}
// */

/*******************************************************************************
 * Address of table
 * /
int main(int argc, char* argv[])
{
	char str[20];

	printf( "%p = %p", str, &str[0] );

	return 0;
}

// */


/*******************************************************************************
 * Function pointers
 * /
typedef int (*operation)(int a, int b);

int calc(int a, int b, operation op)
{
	return op(a, b);
}

int operation_add(int a, int b)
{
	return a + b;
}

int operation_sub(int a, int b)
{
	return a - b;
}

int operation_multi(int a, int b)
{
	return a * b;
}

int main(int argc, char* argv[])
{

	int x = 5;
	int y = 3;
	printf( "%d+%d=%d\n",x, y, calc(x, y, operation_add));
	printf( "%d-%d=%d\n",x, y, calc(x, y, operation_sub));
	printf( "%d*%d=%d\n",x, y, calc(x, y, operation_multi));
			

	return 0;
}

// */


/*******************************************************************************
 * Copying of structures
 * /
struct S
{
	int i;
	char c;
};

void S_print(struct S *s)
{
	printf("%p i=%d, c=%c\n", s, s->i, s->c);
}

int main(int argc, char* argv[])
{
	struct S s1 = {.i=1, .c='a'};
	struct S s2 = s1;

	struct S *p1 = malloc(sizeof(struct S));
	p1->i = 9;
	p1->c = 'z';
	struct S *p2 = p1;
	struct S *p3 = p2;
	*p3 = *p1;

	printf("p1=%p, p2=%p, p3=%p\n", p1, p2, p3);

	S_print(&s1);
	S_print(&s2);
	S_print(p1);
	S_print(p2);
	S_print(p3);

	s1.i = 10;
	s1.c = 'A';
	p1->i = 50;
	p1->c = 'Z';

	S_print(&s1);
	S_print(&s2);
	S_print(p1);
	S_print(p2);
	S_print(p3);


	return 0;
}
// */


/*******************************************************************************
 * Passing pointers tests
 * /
struct s
{
	int i;
	char c;
};
void f(struct s *sptr)
{
	printf("f(%p)\n", sptr);
	printf("+--i=%d\n", sptr->i);
	printf("+--c=%c\n", sptr->c);

	printf("-- change values --\n");
	sptr->i+=10;
	sptr->c-=32;
	printf("+--i=%d\n", sptr->i);
	printf("+--c=%c\n", sptr->c);

	sptr = malloc(sizeof(struct s));
	sptr->i = 100;
	sptr->c = 'z';

}

int main(int argc, char* argv[])
{
	struct s ss;
	ss.i = 1;
	ss.c = 'a';

	struct s *ssptr = malloc(sizeof(struct s));
	ssptr->i = 2;
	ssptr->c = 'b';
	
	printf("ptrs: ss=%p ssptr=%p\n", &ss, ssptr);

	f(&ss);
	f(ssptr);

	printf("second call\n");

	f(&ss);
	f(ssptr);


	return 0;
}
// */


/*******************************************************************************
 * Static and non static string test
 * /
int main(int argc, char* argv[])
{
	char dupa[] = "dupa";
	static char sdupa[] = "sdupa";

	printf("%s\n%s\n%p\n%p\n", dupa, sdupa, dupa, sdupa);

	return 0;
}
// */


